"""
Author: Alberto Dalla Libera
POLY kernel VS volterra MPK kernel
"""

import torch
import torch.utils.data
import numpy as np
import Utils_test as utils
import GP_prior.GP_prior as GP
import GP_prior.Stationary_GP as Stat_GP
import GP_prior.Sparse_GP as Sparse_GP
import Loss.Gaussian_likelihood as loss
import Utils.Parameters_covariance_functions as cov
import matplotlib.pyplot as plt

dtype = torch.float64
device = torch.device('cpu')
mean_input_tr = 0.
std_input_tr = 0.5
mean_input_test = 0.
std_input_test = 1.
std_noise = 0.1
num_samples = 500

##################################
print('\n---- Generate data ----')
# generate training data
x_tr = mean_input_tr + std_input_tr*np.random.randn(num_samples,7)
y_tr = utils.get_poly_SPL_output(x_tr)
y_noisy_tr = (y_tr.reshape([-1]) + std_noise*np.random.randn(y_tr.size)).reshape([-1,1])
# generate test data
x_test = mean_input_test + std_input_test*np.random.randn(num_samples,7)
y_test = utils.get_poly_SPL_output(x_test)
y_noisy_test = (y_test.reshape([-1]) + std_noise*np.random.randn(y_test.size)).reshape([-1,1])
# print num data
_, num_dims = x_tr.shape
print('input data tr shape: ', x_tr.shape)
print('output data tr shape: ', y_noisy_tr.shape)
print('input data test shape: ', x_test.shape)
print('output data test shape: ', y_noisy_test.shape)



#########################################
print('\n---- Initialize the models ----')
# ---- POLY model ----
poly_deg = 3
active_dims = np.arange(num_dims)
sigma_n_init = np.ones(1)
flg_offset = True
# Sigma with ARD
Sigma_function = cov.diagonal_covariance_ARD
Sigma_f_additional_par_list = []
Sigma_pos_par_init = np.ones(num_dims+1)
flg_train_Sigma_pos_par = True
Sigma_free_par_init = None
flg_train_Sigma_free_par = False
m_POLY = Sparse_GP.Poly_GP(active_dims, poly_deg,
                           sigma_n_init=sigma_n_init, flg_train_sigma_n=True,
                           Sigma_function=Sigma_function, Sigma_f_additional_par_list=Sigma_f_additional_par_list, 
                           Sigma_pos_par_init=Sigma_pos_par_init, flg_train_Sigma_pos_par=flg_train_Sigma_pos_par,
                           Sigma_free_par_init=Sigma_free_par_init, flg_train_Sigma_free_par=flg_train_Sigma_free_par,
                           flg_offset=flg_offset,
                           scale_init=np.ones(1), flg_train_scale=False,
                           name='POLY', dtype=dtype, sigma_n_num=None, device=device)
# ---- Volterra MPK model ----
poly_deg = 3
active_dims = np.arange(num_dims)
sigma_n_init = np.ones(1)
Sigma_pos_par_init_list = [np.ones([deg+1, num_dims+1]) if deg==0 else np.ones([deg+1, num_dims]) for deg in range(poly_deg)]
flg_train_Sigma_pos_par_list = [True for deg in range(poly_deg)]
m_MPK = Sparse_GP.get_Volterra_MPK_GP(active_dims, poly_deg,
                                      sigma_n_init=sigma_n_init, flg_train_sigma_n=True,
                                      Sigma_pos_par_init_list=Sigma_pos_par_init_list, flg_train_Sigma_pos_par_list=flg_train_Sigma_pos_par_list,
                                      name='VOLTERRA MPK', dtype=torch.float64, sigma_n_num=None, device=device)



####################################
print('\n---- Train the models ----')
# set optimization parameters
N_epoch = 1501
N_epoch_print = 100
batch_size = num_samples
# move data in torch
x_tr_tc = torch.tensor(x_tr, dtype=dtype, device=device)
y_noisy_tr_tc = torch.tensor(y_noisy_tr.reshape([-1,1]), dtype=dtype, device=device)
x_test_tc = torch.tensor(x_test, dtype=dtype, device=device)
# get a trainloader
tr_dataset = torch.utils.data.TensorDataset(x_tr_tc, y_noisy_tr_tc)
trainloader = torch.utils.data.DataLoader(tr_dataset, batch_size=batch_size, shuffle=True)
print('\nTRAIN POLY MODEL')
# get and optimizer
optimizer = torch.optim.Adam(m_POLY.parameters(), lr=0.01)
# define the criterion
criterion = loss.Marginal_log_likelihood()
m_POLY.fit_model(trainloader=trainloader, 
                 optimizer=optimizer, criterion=criterion,
                 N_epoch=N_epoch, N_epoch_print=N_epoch_print,
                 f_saving_model=None, f_print=None)
print('\nTRAIN VOLTERRA MPK MODEL')
# get and optimizer
optimizer = torch.optim.Adam(m_MPK.parameters(), lr=0.01)
# define the criterion
criterion = loss.Marginal_log_likelihood()
m_MPK.fit_model(trainloader=trainloader, 
                 optimizer=optimizer, criterion=criterion,
                 N_epoch=N_epoch, N_epoch_print=N_epoch_print,
                 f_saving_model=None, f_print=None)




####################################
print('\n---- Test the models ----')
# get the POLY posterior
y_tr_hat_tc_POLY, _, alpha_POLY, m_X_POLY, K_X_inv_POLY = m_POLY.get_estimate(x_tr_tc, y_noisy_tr_tc, x_tr_tc, flg_return_K_X_inv=True)
y_test_hat_tc_POLY, _, _ = m_POLY.get_estimate_from_alpha(x_tr_tc, x_test_tc, alpha_POLY, K_X_inv=K_X_inv_POLY)
# get the MPK posterior
y_tr_hat_tc_MPK, _, alpha_MPK, m_X_MPK, K_X_inv_MPK = m_MPK.get_estimate(x_tr_tc, y_noisy_tr_tc, x_tr_tc, flg_return_K_X_inv=True)
y_test_hat_tc_MPK, _, _ = m_MPK.get_estimate_from_alpha(x_tr_tc, x_test_tc, alpha_MPK, K_X_inv=K_X_inv_MPK)
# move data to numpy and plot the results
y_tr_hat_POLY = y_tr_hat_tc_POLY.detach().cpu().numpy().reshape([-1])
y_test_hat_POLY = y_test_hat_tc_POLY.detach().cpu().numpy().reshape([-1])
y_tr_hat_MPK = y_tr_hat_tc_MPK.detach().cpu().numpy().reshape([-1])
y_test_hat_MPK = y_test_hat_tc_MPK.detach().cpu().numpy().reshape([-1])
print('\nMSE POLY tr:', utils.get_MSE(y_tr.squeeze(), y_tr_hat_POLY.squeeze()))
print('\nMSE MPK tr:', utils.get_MSE(y_tr.squeeze(), y_tr_hat_MPK.squeeze()))
print('\nMSE POLY test:', utils.get_MSE(y_test.squeeze(), y_test_hat_POLY.squeeze()))
print('\nMSE MPK test:', utils.get_MSE(y_test.squeeze(), y_test_hat_MPK.squeeze()))