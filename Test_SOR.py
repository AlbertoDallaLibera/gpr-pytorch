"""
Author: Alberto Dalla Libera
Example with Subset Of Regressors model
"""

import torch
import torch.utils.data
import numpy as np
import Utils_test as utils
import GP_prior.GP_prior as GP
import GP_prior.Stationary_GP as Stat_GP
import GP_prior.Sparse_GP as Sparse_GP
import Loss.Gaussian_likelihood as loss
import Utils.Parameters_covariance_functions as cov
import matplotlib.pyplot as plt

dtype = torch.float64
device = torch.device('cpu')

##################################
print('\n---- Generate data ----')
# get data
x_max = 10
x_sampling = 0.1
num_sinusoids = 20
omega_max = 5.
amp_max = 2.
std_noise = .1
x = np.arange(-x_max,x_max, x_sampling)
y = utils.get_sum_of_sinusoids(t=x,
	                           num_sinusoids=num_sinusoids,
	                           omega_max=omega_max,
	                           amp_max=amp_max)
# y += 0.01*x**2 -x +1
y_noisy = y + std_noise*np.random.randn(y.size)
num_samples = x.size
# get training and test samples
perm_indices = np.random.permutation(np.arange(0,num_samples,1))
tr_indices = perm_indices[:int(num_samples/2)]
test_indices = perm_indices[int(num_samples/2):]
# tr_indices = np.arange(0,100)
# test_indices = np.arange(100, num_samples)
x_tr = x[tr_indices]
y_tr = y[tr_indices]
y_noisy_tr = y_noisy[tr_indices]
x_test = x[test_indices]
y_test = y[test_indices]
y_noisy_test = y_noisy[test_indices]
# print num data
num_samples_tr = x_tr.shape[0]
num_samples_test = x_test.shape[0]
print('num data tr: ', num_samples_tr)
print('num data test: ', num_samples_test)



#########################################
print('\n---- Initialize the model ----')
# RBF model
active_dims = np.arange(0,1)
lengthscales_init = np.ones(1)
sigma_n_init = 1.
m_RBF = Stat_GP.RBF(active_dims=active_dims,
                    lengthscales_init=lengthscales_init, flg_train_lengthscales=True,
                    sigma_n_init=sigma_n_init, flg_train_sigma_n=True,
                    scale_init=np.ones(1), flg_train_scale=True,
                    name='RBF', dtype=dtype, sigma_n_num=None, device=device)
# POLY model
poly_deg = 2
flg_offset = True
# Sigma with ARD
Sigma_function = cov.diagonal_covariance_ARD
Sigma_f_additional_par_list = []
Sigma_pos_par_init = np.ones(2)
flg_train_Sigma_pos_par = True
Sigma_free_par_init = None
flg_train_Sigma_free_par = False
m_POLY = Sparse_GP.Poly_GP(active_dims, poly_deg,
                           sigma_n_init=None, flg_train_sigma_n=False,
                           Sigma_function=Sigma_function, Sigma_f_additional_par_list=Sigma_f_additional_par_list, 
                           Sigma_pos_par_init=Sigma_pos_par_init, flg_train_Sigma_pos_par=flg_train_Sigma_pos_par,
                           Sigma_free_par_init=Sigma_free_par_init, flg_train_Sigma_free_par=flg_train_Sigma_free_par,
                           flg_offset=flg_offset,
                           scale_init=np.ones(1), flg_train_scale=False,
                           name='POLY', dtype=dtype, sigma_n_num=None, device=device)
m = GP.Sum_Independent_GP(*[m_RBF,m_POLY])
# m = m_RBF
m_SOR = Sparse_GP.get_SOR_GP(m)




####################################
print('\n---- Train the exact model ----')
# set optimization parameters
N_epoch = 2000
N_epoch_print = 100
batch_size = num_samples_tr
# move data in torch
x_tc = torch.tensor(x.reshape([-1,1]), dtype=dtype, device=device)
x_tr_tc = torch.tensor(x_tr.reshape([-1,1]), dtype=dtype, device=device)
y_noisy_tr_tc = torch.tensor(y_noisy_tr.reshape([-1,1]), dtype=dtype, device=device)
x_test_tc = torch.tensor(x_test.reshape([-1,1]), dtype=dtype, device=device)
# get a trainloader
tr_dataset = torch.utils.data.TensorDataset(x_tr_tc.reshape([-1,1]), y_noisy_tr_tc)
trainloader = torch.utils.data.DataLoader(tr_dataset, batch_size=batch_size, shuffle=True)
# get the optimizer
optimizer = torch.optim.Adam(m_SOR.parameters(), lr=0.01)
# define the criterion
criterion = loss.Marginal_log_likelihood()
m_SOR.fit_model(trainloader=trainloader, 
            optimizer=optimizer, criterion=criterion,
            N_epoch=N_epoch, N_epoch_print=N_epoch_print,
            f_saving_model=None, f_print=None)


####################################
print('\n---- Derive the approximated model ----')

# # Randomly select a subset of regressors
# # num_regressors = num_samples_tr
# num_regressors = 40
# # reg_indices = np.random.permutation(np.arange(0,num_samples_tr))[0:num_regressors]
# reg_indices = np.arange(0,num_regressors)
# U = x_tr[reg_indices].reshape([num_regressors,1])
# # set the regressors
# m_SOR.init_inducing_inputs(U,flg_train_inducing_inputs=False)
threshold = .5
with torch.no_grad():
    reg_indices = m_SOR.set_inducing_inputs_from_data(x_tr_tc, y_noisy_tr_tc, threshold, flg_trainable=True)
U_pre_tr = m_SOR.U.clone().detach().cpu().numpy().squeeze()


####################################
print('\n---- Test the model before training the SOR ----')
# get the posterior
y_hat_tc, var_hat_tc, _, _ = m_SOR.get_estimate(x_tr_tc, y_noisy_tr_tc, x_tc)
y_hat_SOR_tc, var_hat_SOR_tc, _, _ = m_SOR.get_SOR_estimate(x_tr_tc, y_noisy_tr_tc, x_tc)
y_hat_SOD_tc, var_hat_SOD_tc, _, _ = m_SOR.get_estimate(x_tr_tc[reg_indices,:], y_noisy_tr_tc[reg_indices,:], x_tc)



####################################
print('\n---- Train the regressors ----')
N_epoch = 5000
optimizer = torch.optim.Adam(m_SOR.parameters(), lr=0.01)
m_SOR.fit_SOR_model(trainloader=trainloader, 
                    optimizer=optimizer, criterion=criterion,
                    N_epoch=N_epoch, N_epoch_print=N_epoch_print,
                    f_saving_model=None, f_print=None)
U_post_tr = m_SOR.U.clone().detach().cpu().numpy().squeeze()


####################################
print('\n---- Test the SOR approximation after trining the SOR ----')
y_hat_SOR2_tc, var_hat_SOR2_tc, _, _ = m_SOR.get_SOR_estimate(x_tr_tc, y_noisy_tr_tc, x_tc)


###################################
print('\n---- Plot results ----')
# move data to numpy and plot the results
y_hat = y_hat_tc.detach().cpu().numpy().squeeze()
std = np.sqrt(var_hat_tc.detach().cpu().numpy())
y_SOR_hat = y_hat_SOR_tc.detach().cpu().numpy().squeeze()
std_SOR = np.sqrt(var_hat_SOR_tc.detach().cpu().numpy())
y_SOR2_hat = y_hat_SOR2_tc.detach().cpu().numpy().squeeze()
std_SOR2 = np.sqrt(var_hat_SOR2_tc.detach().cpu().numpy())
y_SOD_hat = y_hat_SOD_tc.detach().cpu().numpy().squeeze()
std_SOD = np.sqrt(var_hat_SOD_tc.detach().cpu().numpy())
print('\nMSE full GP:', utils.get_MSE(y, y_hat))
print('\nMSE SOR:', utils.get_MSE(y, y_SOR_hat))
print('\nMSE SOR2:', utils.get_MSE(y, y_SOR2_hat))
print('\nMSE SOD:', utils.get_MSE(y, y_SOD_hat))
plt.figure()
plt.grid()
plt.plot(x,y, label='$f$')
plt.plot(x,y_hat, label='$\\hat{f}$', color='b')
plt.fill(np.concatenate([x,np.flip(x)]),np.concatenate([y_hat+3*std,np.flip(y_hat-3*std)]), 'b', alpha=0.3)
plt.plot(x,y_SOR_hat, label='$\\hat{f}_{SOR}$', color='m')
plt.fill(np.concatenate([x,np.flip(x)]),np.concatenate([y_SOR_hat+3*std_SOR,np.flip(y_SOR_hat-3*std_SOR)]), 'm', alpha=0.3)
plt.plot(x,y_SOR2_hat, label='$\\hat{f}_{SOR} post$', color='r')
plt.fill(np.concatenate([x,np.flip(x)]),np.concatenate([y_SOR2_hat+3*std_SOR2,np.flip(y_SOR2_hat-3*std_SOR2)]), 'r', alpha=0.3)
plt.plot(x,y_SOD_hat, label='$\\hat{f}_{SOD}$', color='g')
plt.fill(np.concatenate([x,np.flip(x)]),np.concatenate([y_SOD_hat+3*std_SOD,np.flip(y_SOD_hat-3*std_SOD)]), 'g', alpha=0.3)
# plt.plot(U,y_noisy_tr[reg_indices], 'x', label='$U start$')
plt.plot(U_pre_tr,y_noisy_tr[reg_indices], 'X', label='$U_{tr}$', color='k')
plt.plot(U_post_tr,y_noisy_tr[reg_indices], 'x', label='$U_{tr} post$', color='k')
# plt.plot(x_tr,y_noisy_tr, 'x', label='$y^{noisy}_{tr}$')
plt.legend()
plt.show()