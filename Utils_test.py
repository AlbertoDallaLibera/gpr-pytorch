"""
Author: Alberto Dalla Libera
Utils function for the test
"""

import numpy as np
import GP_prior.Stationary_GP as Stat_GP
import matplotlib.pyplot as plt



def get_sum_of_sinusoids(t, num_sinusoids, omega_max, amp_max):
    """
    Generates num_sinusoids with amplitude and angular velocities ranging 
    [-amp_max,+amp_max] and [-omega_max,+omega_max]
    """
    amplitude_list = 2*amp_max*(np.random.rand(num_sinusoids)-0.5)
    omega_list = 2*omega_max*(np.random.rand(num_sinusoids)-0.5)
    out = np.zeros(t.shape)
    for sin_index in range(num_sinusoids):
        out += amplitude_list[sin_index]*np.sin(omega_list[sin_index]*t)
    return out


def get_RBF_GP_data(X, lengthscales, scale):
    """
    Generates data from a multivariate gaussian with RBF covariance
    """
    # generate the covariance
    N, x_dim = X.shape
    active_dims = np.arange(0,x_dim)
    m = Stat_GP.RBF(active_dims=active_dims,
                    lengthscales_init=lengthscales, flg_train_lengthscales=False,
                    sigma_n_init=None, flg_train_sigma_n=False,
                    scale_init=scale, flg_train_scale=False,
                    name='', dtype=X.dtype, sigma_n_num=None, device=X.device)
    cov = m.get_covariance(X)
    # sample data
    y = np.random.multivariate_normal(np.zeros(N), cov, size=None, check_valid='warn', tol=1e-8)
    return y


def get_poly_SPL_output(u):
    """
    Returns the input output numpy arrays relative to the system proposed 
    in Spinelli,Pirrodi, Lovera. The system evolution is described by a 
    polynomial in u with degree 3
    - u.shape = [num_dat, 6]
    - u[t, :] = [u_t,...,u_{t-6};...]
    """
    #get the outputs
    output = u[:,0] + 0.6*u[:,1] + 0.35*(u[:,2]+u[:,4]) \
                   -0.25*(u[:,3]**2) + 0.2*(u[:,5] + u[:,6]) +0.9*u[:,3] \
                   +0.25*u[:,0]*u[:,1] + 0.75*(u[:,2]**3) \
                   -u[:,1]*u[:,2] \
                   + 0.5*(u[:,0]**2 + u[:,0]*u[:,2] + u[:,1]*u[:,3])
    return output.reshape([-1,1])


def get_MSE(y, y_hat):
    """
    Returns the MSE
    """
    return ((y-y_hat)**2).mean()

def plot_signals(x, y, f_mean_hat=None, f_var_hat=None, y_noisy=None, x_tr=None, y_tr=None, title=None):
    """
    Plot
    """
    plt.figure()
    plt.grid()
    plt.plot(x,y, label='$f$', color='black')
    if y_noisy is not None:
        plt.plot(x,y_noisy, 'x', label='$y samples$', color='black')
    if f_mean_hat is not None:
        plt.plot(x,f_mean_hat, label='$\\hat{f}$', color='blue')
    if x_tr is not None:
        plt.plot(x_tr,y_tr, 'x', label='$y samples$', color='blue')
    if f_var_hat is not None:
        std = np.sqrt(f_var_hat)
        plt.fill(np.concatenate([x, np.flip(x)]),
                np.concatenate([f_mean_hat + 3 * std, np.flip(f_mean_hat) - 3 * np.flip(std)]), color='blue', alpha=0.2)
    plt.xlabel('$x$')
    plt.ylabel('output')
    plt.legend()
    if title is not None:
        plt.title(title)