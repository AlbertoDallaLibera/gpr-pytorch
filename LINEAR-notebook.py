"""
Author: Alberto Dalla Libera
Example with linear kernel
"""

import torch
import torch.utils.data
import numpy as np
import Utils_test as utils
import GP_prior.Sparse_GP as Sparse_GP
import Loss.Gaussian_likelihood as loss
import matplotlib.pyplot as plt
import Utils.Parameters_covariance_functions as cov

# set data type and device
dtype = torch.float64
device = torch.device('cpu')
np.random.seed(0)

# set program parameters
flg_tr_data_generation = 'random' # 'random' or 'split'
tr_perc = 0.2 # 0=no training data, 1 = all training data

print('\n---- Generate data ----')
# get data
x_max = 10
x_sampling = 0.025
num_sinusoids = 20
omega_max = 5.
amp_max = 2.
std_noise = .5
# input
x = np.arange(-x_max,x_max, x_sampling)
# output
y = 0.1*x**2*np.sin(x)**2 + .2*x
# noisy output
y_noisy = y + std_noise*np.random.randn(y.size)
# plot data
utils.plot_signals(x=x,
                   y=y,
                   y_noisy=y_noisy,
                   title='Data')
plt.savefig('data.png')
# get training and test samples
num_samples = x.size
num_samples_tr = int(num_samples*tr_perc)
num_samples_test = num_samples - num_samples_tr
if flg_tr_data_generation == 'random':
    perm_indices = np.random.permutation(np.arange(0,num_samples,1))
else:
    perm_indices = np.arange(0,num_samples,1)
tr_indices = perm_indices[:num_samples_tr]
test_indices = perm_indices[num_samples_tr:]
x_tr = x[tr_indices]
y_tr = y[tr_indices]
y_noisy_tr = y_noisy[tr_indices]
x_test = x[test_indices]
y_test = y[test_indices]
y_noisy_test = y_noisy[test_indices]
# move data to torch
x_tc = torch.tensor(x, dtype=dtype, device=device).reshape([-1,1])
x_tr_tc = torch.tensor(x_tr, dtype=dtype, device=device).reshape([-1,1])
x_test_tc = torch.tensor(x_test, dtype=dtype, device=device).reshape([-1,1])
y_noisy_tr_tc = torch.tensor(y_noisy_tr, dtype=dtype, device=device).reshape([-1,1])
# print num data
print('num data tr: ', num_samples_tr)
print('num data test: ', num_samples_test)


print('\n---- Initialize the model ----')
active_dims = np.arange(0,2)
lengthscales_init = np.ones(1)
sigma_n_init = 1.
Sigma_function = cov.diagonal_covariance_ARD
Sigma_f_additional_par_list = []
Sigma_pos_par_init = np.ones(2)
flg_train_Sigma_pos_par = True
Sigma_free_par_init = None
flg_train_Sigma_free_par = False
m = Sparse_GP.Linear_GP(active_dims, f_transform=lambda x:torch.concatenate([x**2*torch.sin(x)**2, x], 1), f_add_par_list=[],
                  sigma_n_init=sigma_n_init, flg_train_sigma_n=True,
                  Sigma_function=Sigma_function, Sigma_f_additional_par_list=Sigma_f_additional_par_list, 
                  Sigma_pos_par_init=Sigma_pos_par_init, flg_train_Sigma_pos_par=flg_train_Sigma_pos_par,
                  Sigma_free_par_init=Sigma_free_par_init, flg_train_Sigma_free_par=flg_train_Sigma_free_par,
                  flg_offset=False,
                  scale_init=np.ones(1), flg_train_scale=False,
                  name='', dtype=torch.float64, sigma_n_num=None, device=None)
# get prior
mean_x_prior = m.get_mean(x_tc).detach().cpu().numpy().reshape([-1])
var_prior = m.get_diag_covariance(x_tc).detach().cpu().numpy().reshape([-1])
# plot data VS prior
utils.plot_signals(x=x,
                   y=y,
                   f_mean_hat=mean_x_prior,
                   #y_noisy=y_noisy,
                   f_var_hat=var_prior,
                   title='Data VS prior')
plt.savefig('init.png')

print('\n---- Train the model hyperparameters----')
# set optimization parameters
N_epoch = 1500
N_epoch_print = 500
batch_size = num_samples_tr
# get a trainloader
tr_dataset = torch.utils.data.TensorDataset(x_tr_tc.reshape([-1,1]), y_noisy_tr_tc)
trainloader = torch.utils.data.DataLoader(tr_dataset, batch_size=batch_size, shuffle=True)
# get and optimizer
optimizer = torch.optim.Adam(m.parameters(), lr=0.01)
# define the criterion
criterion = loss.Marginal_log_likelihood()
# optimize the prior
m.fit_model(trainloader=trainloader, 
            optimizer=optimizer, criterion=criterion,
            N_epoch=N_epoch, N_epoch_print=N_epoch_print,
            f_saving_model=None, f_print=None)
# get updated prior
mean_x_prior_updated = m.get_mean(x_tc).detach().cpu().numpy().reshape([-1])
var_prior_updated = m.get_diag_covariance(x_tc).detach().cpu().numpy().reshape([-1])
# plot data VS updated prior
utils.plot_signals(x=x,
                   y=y,
                   f_mean_hat=mean_x_prior_updated,
                   #y_noisy=y_noisy,
                   f_var_hat=var_prior_updated,
                   title='Data VS trained prior')
plt.savefig('trained_prior.png')

print('\n---- Get posterior and estimations ----')
# get the posterior
y_tr_hat_tc, _, alpha, m_X_tr, K_X_inv = m.get_estimate(x_tr_tc, y_noisy_tr_tc, x_tr_tc, flg_return_K_X_inv=True)
y_test_hat_tc, _ , _ = m.get_estimate_from_alpha(x_tr_tc, x_test_tc, alpha, K_X_inv=K_X_inv, )
y_hat_tc, var_tc, m_X_tc = m.get_estimate_from_alpha(x_tr_tc, x_tc, alpha, K_X_inv=K_X_inv)
# move data to numpy and plot the results
y_hat = y_hat_tc.detach().cpu().numpy().reshape([-1])
var = var_tc.detach().cpu().numpy().reshape([-1])
m_X = m_X_tc.detach().cpu().numpy().reshape([-1])
y_tr_hat = y_tr_hat_tc.detach().cpu().numpy().reshape([-1])
y_test_hat = y_test_hat_tc.detach().cpu().numpy().reshape([-1])
print('\nMSE:', utils.get_MSE(y.squeeze(), y_hat.squeeze()))
print('\nMSE tr:', utils.get_MSE(y_tr.squeeze(), y_tr_hat.squeeze()))
print('\nMSE test:', utils.get_MSE(y_test.squeeze(), y_test_hat.squeeze()))
# plot data VS posterior
utils.plot_signals(x=x,
                   y=y,
                   f_mean_hat=y_hat,
                   #y_noisy=y_noisy,
                   f_var_hat=var,
                   x_tr=x_tr,
                   y_tr=y_noisy_tr,
                   title='Data VS Posterior')
plt.savefig('posterior.png')
plt.show()