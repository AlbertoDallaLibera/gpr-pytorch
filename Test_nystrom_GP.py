"""
Author: Alberto Dalla Libera
Example with nystrom GP
"""

import torch
import torch.utils.data
import numpy as np
import Utils_test as utils
import GP_prior.Stationary_GP as Stat_GP
import GP_prior.Sparse_GP as Sparse_GP
import Loss.Gaussian_likelihood as loss
import matplotlib.pyplot as plt
import Utils

dtype = torch.float64
device = torch.device('cpu')
flg_sin = False #if true data are generated from a random sum of sinusoids
x_dim = 1

##################################
print('\n---- Generate data ----')
if flg_sin:
    # generate samples from sum  of sinusoids
    x_dim = 1
    x_max = 10
    x_sampling = 0.1
    num_sinusoids = 20
    omega_max = 5.
    amp_max = 2.
    std_noise = .1
    x = np.arange(-x_max, x_max, x_sampling)
    y = utils.get_sum_of_sinusoids(t=x,
                                   num_sinusoids=num_sinusoids,
                                   omega_max=omega_max,
                                   amp_max=amp_max)
    x = np.arange(-x_max, x_max, x_sampling).reshape([-1, 1])
else:
    # generate samples from an RBF GP
    if x_dim == 1:
        lengthscales = np.array([.5])
        x_max = 10
        x_sampling = 0.1
        x = np.arange(-x_max, x_max, x_sampling).reshape([-1, x_dim])
    else:
        x_dim = 2
        lengthscales = np.array([1.15, .75])
        x_max = 2
        x_sampling = 0.1
        x1 = np.arange(-x_max,x_max, x_sampling)
        x2 = np.arange(-x_max,x_max, x_sampling)
        x1v, x2v = np.meshgrid(x1, x2)
        x = np.concatenate([x1v.reshape([-1,1]), x2v.reshape([-1,1])], 1)
    scale = 1.
    std_noise = .1
    y = utils.get_RBF_GP_data(torch.tensor(
        x, device=device, dtype=dtype), lengthscales, scale)
y_noisy = y + std_noise * np.random.randn(y.size)
num_samples = x.shape[0]
# get training and test samples
# perm_indices = np.random.permutation(np.arange(0,num_samples,1))
perm_indices = np.arange(0, num_samples, 1)
tr_indices = perm_indices[:int(num_samples / 2)]
test_indices = perm_indices[int(num_samples / 2):]
x_tr = x[tr_indices, :]
y_tr = y[tr_indices]
y_noisy_tr = y_noisy[tr_indices]
x_test = x[test_indices]
y_test = y[test_indices]
y_noisy_test = y_noisy[test_indices]
# print num data
num_samples_tr = x_tr.shape[0]
num_samples_test = x_test.shape[0]
print('num data tr: ', num_samples_tr)
print('num data test: ', num_samples_test)


#########################################
print('\n---- Initialize the model ----')
active_dims = np.arange(0, x_dim)
lengthscales_init = np.ones(x_dim)
sigma_n_init = 1.
m = Stat_GP.RBF(active_dims=active_dims,
                lengthscales_init=lengthscales_init, flg_train_lengthscales=True,
                sigma_n_init=sigma_n_init, flg_train_sigma_n=True,
                scale_init=np.ones(1), flg_train_scale=True,
                name='', dtype=dtype, sigma_n_num=None, device=device)


####################################
print('\n---- Train the model ----')
# set optimization parameters
N_epoch = 1501
batch_size = num_samples_tr
lr = 0.01
N_epoch_print = 100
# batch_size = 100
# N_epoch = int(15001/(int(num_samples_tr/batch_size)))
# lr = 0.001
# N_epoch_print = 100
# move data in torch
x_tc = torch.tensor(x.reshape([-1, x_dim]), dtype=dtype, device=device)
x_tr_tc = torch.tensor(x_tr.reshape([-1, x_dim]), dtype=dtype, device=device)
y_noisy_tr_tc = torch.tensor(y_noisy_tr.reshape(
    [-1, 1]), dtype=dtype, device=device)
x_test_tc = torch.tensor(x_test.reshape(
    [-1, x_dim]), dtype=dtype, device=device)
# get a trainloader
print('x_tr_tc', x_tr_tc.shape)
print('y_noisy_tr_tc', y_noisy_tr_tc.shape)
tr_dataset = torch.utils.data.TensorDataset(x_tr_tc, y_noisy_tr_tc)
trainloader = torch.utils.data.DataLoader(
    tr_dataset, batch_size=batch_size, shuffle=True)
# get and optimizer
optimizer = torch.optim.Adam(m.parameters(), lr=lr)
# define the criterion
criterion = loss.Marginal_log_likelihood()
m.fit_model(trainloader=trainloader,
            optimizer=optimizer, criterion=criterion,
            N_epoch=N_epoch, N_epoch_print=N_epoch_print,
            f_saving_model=None, f_print=None)


####################################
print('\n---- Get the Nystrom approximation ----')
# get the nystrom model
# R_max = num_samples_tr
R_max = num_samples
# R_max = 5
nystrom_init_dict = {}
# nystrom_init_dict['sigma_n_init'] = np.ones(1)
# nystrom_init_dict['flg_train_sigma_n'] = True
nystrom_init_dict['sigma_n_init'] = np.sqrt(
    m.get_sigma_n_2().detach().cpu().numpy())
nystrom_init_dict['flg_train_sigma_n'] = False
nystrom_init_dict['f_mean'] = None
nystrom_init_dict['f_mean_add_par_dict'] = {}
nystrom_init_dict['pos_par_mean_init'] = None
nystrom_init_dict['flg_train_pos_par_mean'] = False
nystrom_init_dict['free_par_mean_init'] = None
nystrom_init_dict['flg_train_free_par_mean'] = False
nystrom_init_dict['flg_train_Sigma_free_par'] = False
nystrom_init_dict['flg_offset'] = False
nystrom_init_dict['scale_init'] = np.ones(1)
nystrom_init_dict['flg_train_scale'] = False
nystrom_init_dict['sigma_n_num'] = None
# m_nystrom = Sparse_GP.get_nystrom_GP(m, x_tr_tc, R_max, nystrom_init_dict, 0.000001)
m_nystrom = Sparse_GP.get_nystrom_GP(m, x_tc, R_max, nystrom_init_dict, 0.01)
# m_nystrom = Sparse_GP.get_nystrom_GP(m, x_test_tc, R_max, nystrom_init_dict, 0.000001)
# # train the nystrom model
# # N_epoch = 1501
# # batch_size = num_samples_tr
# # lr = 0.01
# # N_epoch_print = 100
# optimizer = torch.optim.Adam(m_nystrom.parameters(), lr=lr)
# criterion = loss.Marginal_log_likelihood()
# m_nystrom.fit_model(trainloader=trainloader,
#                     optimizer=optimizer, criterion=criterion,
#                     N_epoch=N_epoch, N_epoch_print=N_epoch_print,
#                     f_saving_model=None, f_print=None)
# # m_nystrom.fit_model_inv_lemma(trainloader=trainloader,
# #                               optimizer=optimizer, criterion=criterion,
# #                               N_epoch=N_epoch, N_epoch_print=N_epoch_print,
# #                               f_saving_model=None, f_print=None)
w = m_nystrom.get_parameters(x_tr_tc, y_noisy_tr_tc)
w_inv_lemma, Sigma_w = m_nystrom.get_parameters_inv_lemma(
    x_tr_tc, y_noisy_tr_tc)
L_sigma_w = torch.linalg.cholesky(Sigma_w)
N_w = 200
with torch.no_grad():
    w_sampled = w_inv_lemma + torch.matmul(L_sigma_w, torch.randn([N_w, w_inv_lemma.shape[0], 1],
                                                                  dtype=m_nystrom.dtype,
                                                                  device=m_nystrom.device))
    w_sampled_numpy = w_sampled.squeeze().t().detach().cpu().numpy()


####################################
print('\n---- Test the model ----')
with torch.no_grad():
    # get the posterior full GP
    y_tr_hat_tc, _, alpha, m_X, K_X_inv = m.get_estimate(x_tr_tc, y_noisy_tr_tc, x_tr_tc, flg_return_K_X_inv=True)
    y_test_hat_tc, _, _ = m.get_estimate_from_alpha(x_tr_tc, x_test_tc, alpha, K_X_inv=K_X_inv, )
    y_hat_tc, var_tc, _ = m.get_estimate_from_alpha(x_tr_tc, x_tc, alpha, K_X_inv=K_X_inv)
    # get the posterior nystrom
    y_tr_hat_tc_nystrom, _, alpha_nystrom, m_X_nystrom, K_X_inv_nystrom = m_nystrom.get_estimate(x_tr_tc, y_noisy_tr_tc, x_tr_tc, flg_return_K_X_inv=True)
    y_test_hat_tc_nystrom, _, _ = m_nystrom.get_estimate_from_alpha(x_tr_tc, x_test_tc, alpha_nystrom, K_X_inv=K_X_inv_nystrom, )
    y_hat_tc_nystrom, var_tc_nystrom, _ = m_nystrom.get_estimate_from_alpha(x_tr_tc, x_tc, alpha_nystrom, K_X_inv=K_X_inv_nystrom)
# move data to numpy
y_hat = y_hat_tc.detach().cpu().numpy().reshape([-1])
var = var_tc.detach().cpu().numpy().reshape([-1])
var_nystrom = var_tc_nystrom.detach().cpu().numpy().reshape([-1])
y_tr_hat = y_tr_hat_tc.detach().cpu().numpy().reshape([-1])
y_test_hat = y_test_hat_tc.detach().cpu().numpy().reshape([-1])
y_hat_nystrom = y_hat_tc_nystrom.detach().cpu().numpy().reshape([-1])
y_tr_hat_nystrom = y_tr_hat_tc_nystrom.detach().cpu().numpy().reshape([-1])
y_test_hat_nystrom = y_test_hat_tc_nystrom.detach().cpu().numpy().reshape([-1])
print('\nMSE full GP:', utils.get_MSE(y.squeeze(), y_hat.squeeze()))
print('\nMSE nystrom:', utils.get_MSE(y.squeeze(), y_hat_nystrom.squeeze()))
# get samples
with torch.no_grad():
    samples = torch.matmul(m_nystrom.get_phi(
        x_tc), w_sampled).detach().cpu().numpy()
# plot the results
plt.figure()
plt.grid()
X_plot = np.arange(0, num_samples, 1)
plt.plot(X_plot, y, label='$y$', color='black')
plt.plot(X_plot, y_hat, label='$y_{hat}$', color='blue')
std = np.sqrt(var)
plt.fill(np.concatenate([X_plot, np.flip(X_plot)]), np.concatenate(
    [y_hat + 3 * std, np.flip(y_hat) - 3 * np.flip(std)]), color='blue', alpha=0.1)
plt.plot(tr_indices, y_noisy_tr, 'x', label='$y^{noisy}_{tr}$')
plt.plot(X_plot, y_hat_nystrom, label='$y_{hat}$ nystrom', color='red')
for i in range(N_w):
    plt.plot(X_plot, samples[i, :, 0], color='red', alpha=0.05)
std_nystrom = np.sqrt(var_nystrom)
plt.fill(np.concatenate([X_plot, np.flip(X_plot)]), np.concatenate(
    [y_hat_nystrom + 3 * std_nystrom, np.flip(y_hat_nystrom) - 3 * np.flip(std_nystrom)]), color='red', alpha=0.1)
plt.legend()
plt.show()
