"""
Author: Alberto Dalla Libera
This file contains the definitions of scaling functions
"""

import torch




def f_get_sign(X_active, pos_par=None, free_par=None, flg_sign_pos=True):
    """
    Returns a vecor containing ones and zeros, depending on the fact that X_active greater or lower than free_par.
    """
    N = X_active.shape[0] 
    if flg_sign_pos:
        return torch.prod(X_active>free_par, 1, keepdim=True, dtype=free_par.dtype).reshape([N,1])
    else:
        return torch.prod(X_active<free_par, 1, keepdim=True, dtype=free_par.dtype).reshape([N,1])




def f_get_sign_abs(X_active, pos_par=None, free_par=None, flg_sign_pos=True):
    """
    Returns a vecor containing zeros and ones, depending on the fact that X is positive or negative.
    """
    N = X_active.shape[0] 
    if flg_sign_pos:
        return torch.prod(torch.abs(X_active)>pos_par, 1, keepdim=True, dtype=pos_par.dtype).reshape([N,1])
    else:
        return torch.prod(torch.abs(X_active)<pos_par, 1, keepdim=True, dtype=pos_par.dtype).reshape([N,1])

