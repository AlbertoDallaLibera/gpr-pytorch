"""
Author: Alberto Dalla Libera
Example with scaling function
"""

import torch
import torch.utils.data
import numpy as np
import Utils_test as utils
import GP_prior.GP_prior as GP
import GP_prior.Stationary_GP as Stat_GP
import GP_prior.Sparse_GP as Sparse_GP
import Loss.Gaussian_likelihood as loss
import Utils.Parameters_covariance_functions as cov
import Utils.Scaling_functions as f_scale
import matplotlib.pyplot as plt

dtype = torch.float64
device = torch.device('cpu')

##################################
print('\n---- Generate data ----')
# get data
x_max = 10
x_sampling = 0.1
num_sinusoids = 20
omega_max = 5.
amp_max = 1.
std_noise = .1
x = np.arange(-x_max,x_max, x_sampling)
y = utils.get_sum_of_sinusoids(t=x,
                               num_sinusoids=num_sinusoids,
                               omega_max=omega_max,
                               amp_max=amp_max)
y[x>0] += 2*x[x>0] + 1
y_noisy = y + std_noise*np.random.randn(y.size)
num_samples = x.size
# get training and test samples
perm_indices = np.random.permutation(np.arange(0,num_samples,1))
tr_indices = perm_indices[:int(num_samples/2)]
test_indices = perm_indices[int(num_samples/2):]
x_tr = x[tr_indices]
y_tr = y[tr_indices]
y_noisy_tr = y_noisy[tr_indices]
x_test = x[test_indices]
y_test = y[test_indices]
y_noisy_test = y_noisy[test_indices]
# print num data
num_samples_tr = x_tr.shape[0]
num_samples_test = x_test.shape[0]
print('num data tr: ', num_samples_tr)
print('num data test: ', num_samples_test)



#########################################
print('\n---- Initialize the model ----')
# RBF model
active_dims = np.arange(0,1)
lengthscales_init = np.ones(1)
sigma_n_init = 1.
m_RBF = Stat_GP.RBF(active_dims=active_dims,
                    lengthscales_init=lengthscales_init, flg_train_lengthscales=True,
                    sigma_n_init=sigma_n_init, flg_train_sigma_n=True,
                    scale_init=np.ones(1), flg_train_scale=True,
                    name='RBF', dtype=dtype, sigma_n_num=None, device=device)
# LIN model scaled
# set the dictionary for initializing the LIN GP
GP_prior_par_dict = {}
GP_prior_par_dict['active_dims'] = active_dims
GP_prior_par_dict['sigma_n_init'] = None
GP_prior_par_dict['flg_train_sigma_n'] =False
GP_prior_par_dict['Sigma_function'] = cov.diagonal_covariance_ARD
GP_prior_par_dict['Sigma_f_additional_par_list'] = []
GP_prior_par_dict['Sigma_pos_par_init'] = np.ones(2)
GP_prior_par_dict['flg_train_Sigma_pos_par'] = True
GP_prior_par_dict['Sigma_free_par_init'] = None
GP_prior_par_dict['flg_train_Sigma_free_par'] = False
GP_prior_par_dict['flg_offset'] = True
GP_prior_par_dict['scale_init'] = np.ones(1)
GP_prior_par_dict['flg_train_scale'] = False
GP_prior_par_dict['name'] = 'LIN scaled'
GP_prior_par_dict['dtype'] = dtype
GP_prior_par_dict['sigma_n_num'] = None
GP_prior_par_dict['device'] = device
GP_prior_class = Sparse_GP.Linear_GP
# set the scaling function
f_scale = f_scale.f_get_sign
active_dims_f_scale = np.array([0])
pos_par_f_init = None
free_par_f_init = np.zeros(1)
# the the scaled GP
m_LIN_scaled = GP.Scale_GP_prior(GP_prior_class, GP_prior_par_dict,
                                 f_scale, active_dims_f_scale,
                                 pos_par_f_init=pos_par_f_init, flg_train_pos_par_f=False,
                                 free_par_f_init=free_par_f_init, flg_train_free_par_f=False,
                                 additional_par_f_list=[True])
# sum the RBF with the LIN GP
m = GP.Sum_Independent_GP(*[m_RBF,m_LIN_scaled])




####################################
print('\n---- Train the model ----')
# set optimization parameters
N_epoch = 1000
N_epoch_print = 50
batch_size = num_samples_tr
# move data in torch
x_tc = torch.tensor(x.reshape([-1,1]), dtype=dtype, device=device)
x_tr_tc = torch.tensor(x_tr.reshape([-1,1]), dtype=dtype, device=device)
y_noisy_tr_tc = torch.tensor(y_noisy_tr.reshape([-1,1]), dtype=dtype, device=device)
x_test_tc = torch.tensor(x_test.reshape([-1,1]), dtype=dtype, device=device)
# get a trainloader
tr_dataset = torch.utils.data.TensorDataset(x_tr_tc.reshape([-1,1]), y_noisy_tr_tc)
trainloader = torch.utils.data.DataLoader(tr_dataset, batch_size=batch_size, shuffle=True)
# get and optimizer
optimizer = torch.optim.Adam(m.parameters(), lr=0.1)
# define the criterion
criterion = loss.Marginal_log_likelihood()
m.fit_model(trainloader=trainloader, 
            optimizer=optimizer, criterion=criterion,
            N_epoch=N_epoch, N_epoch_print=N_epoch_print,
            f_saving_model=None, f_print=None)




####################################
print('\n---- Test the model ----')
# get the posterior
y_tr_hat_tc, _, alpha, m_X, K_X_inv = m.get_estimate(x_tr_tc, y_noisy_tr_tc, x_tr_tc, flg_return_K_X_inv=True)
y_test_hat_tc, _, _ = m.get_estimate_from_alpha(x_tr_tc, x_test_tc, alpha, K_X_inv=K_X_inv)
y_hat_tc, var_tc, _ = m.get_estimate_from_alpha(x_tr_tc, x_tc, alpha, K_X_inv=K_X_inv)
# move data to numpy and plot the results
y_hat = y_hat_tc.detach().cpu().numpy().reshape([-1])
var = var_tc.detach().cpu().numpy().reshape([-1])
y_tr_hat = y_tr_hat_tc.detach().cpu().numpy().reshape([-1])
y_test_hat = y_test_hat_tc.detach().cpu().numpy().reshape([-1])
print('\nMSE:', utils.get_MSE(y.squeeze(), y_hat.squeeze()))
print('\nMSE tr:', utils.get_MSE(y_tr.squeeze(), y_tr_hat.squeeze()))
print('\nMSE test:', utils.get_MSE(y_test.squeeze(), y_test_hat.squeeze()))
plt.figure()
plt.grid()
plt.plot(x,y, label='$f$', color='black')
plt.plot(x,y_hat, label='$\\hat{f}$', color='blue')
std = np.sqrt(var)
plt.fill(np.concatenate([x, np.flip(x)]),
         np.concatenate([y_hat + 3 * std, np.flip(y_hat) - 3 * np.flip(std)]), color='blue', alpha=0.2)
plt.plot(x_tr,y_noisy_tr, 'x', label='$y_{tr}$')
plt.xlabel('$x$')
plt.ylabel('output')
plt.legend()
plt.show()